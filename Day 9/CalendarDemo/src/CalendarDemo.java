import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class CalendarDemo {

	public static void main(String[] args) {
		
		int date, month, year;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter date of birth (dd-MM-yyyy) : ");
		String input = sc.next();
		//System.out.println(input);
		
		LocalDate dob = LocalDate.parse(input, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		LocalDate now = LocalDate.now();
		
		Period difference = Period.between(dob, now);
		System.out.println(difference.getDays() + "-" + difference.getMonths() + "-" + difference.getYears());

	}

}
