package com.hsbc.service;

import com.hsbc.model.*;
import java.util.*;

public interface ItemService {
	static final ItemService itemService = new ItemServiceImpl();
	
	Items createItem(Items item);
	List<Items> fetchItems();
	Items fetchItem(long itemId);
	void deleteItem(String itemName);
	
	static ItemService getItemService() {
		return new ItemServiceImpl();
	}
}
