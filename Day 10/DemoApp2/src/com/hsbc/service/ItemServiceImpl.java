package com.hsbc.service;

import java.util.*;
import com.hsbc.dao.*;
import com.hsbc.model.Items;

public class ItemServiceImpl implements ItemService{
	
	private ItemDao itemDao= ItemDao.getInstance();
	
	@Override
	public Items createItem(Items item) {
		return this.itemDao.saveItem(item);
	}

	@Override
	public List<Items> fetchItems() {
		return this.itemDao.fetchItems();
	}

	@Override
	public void deleteItem(String itemName) {
		this.itemDao.deleteItem(itemName);
		
	}

	@Override
	public Items fetchItem(long itemId) {
		
		return itemDao.fetchItem(itemId);
	}
	

}
