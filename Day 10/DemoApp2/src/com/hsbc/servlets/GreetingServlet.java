package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.hsbc.model.User;

public class GreetingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		HttpSession session = req.getSession();
		
		System.out.println("=================Greeting Session Starts===================");
		
		User user = (User)session.getAttribute("user");
		System.out.println("User : " + user);
		
		System.out.println("Updating the state of the user...");
		session.setAttribute("user", new User("Mayuri", 21));
		
		System.out.println("=================Greeting Session Ends===================");
		
		/*
		 * String firstName = req.getParameter("firstName"); String lastName =
		 * req.getParameter("lastName");
		 * 
		 * LocalDateTime currentDate = LocalDateTime.now();
		 * 
		 * PrintWriter out = resp.getWriter();
		 * 
		 * out.write("<h1>Welcome " + firstName + " " + lastName +
		 * ".The current Date is: " +currentDate.toString()+" </h1>");
		 */
	}
	
	

}
