package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.hsbc.model.User;

public class AgeCalculatorServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		
		HttpSession session = req.getSession();
		
		System.out.println("=============== Age session starts=================");
		
		User user = (User)session.getAttribute("user");
		System.out.println("User : " + user);
		
		System.out.println("=============== Age session ends=================");
		
		/*
		 * String ageString = req.getParameter("dob"); LocalDate dob =
		 * LocalDate.parse(ageString, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		 * 
		 * LocalDate now = LocalDate.now(); Period difference = Period.between(dob,
		 * now); PrintWriter pw = resp.getWriter();
		 * 
		 * pw.write("<h1> You are " + difference.getDays() + " years, " +
		 * difference.getMonths() + " month and " + difference.getYears() +
		 * " days old</h1>"); //System.out.println();
		 */		
	}
	
	

}
