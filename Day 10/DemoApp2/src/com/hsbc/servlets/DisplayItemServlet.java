package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.model.Items;
import com.hsbc.service.ItemService;

public class DisplayItemServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException, NullPointerException {
		
		long itemId = Long.parseLong(req.getParameter("itemId"));
		ItemService itemService = ItemService.getItemService();
		
		Items item = itemService.fetchItem(itemId);
		
		PrintWriter pw = res.getWriter();
		pw.write("<h1>Requested Item : </h1><br>");
		pw.write(item.getItemId() + "\t" + item.getItemName() + "\t" + item.getPrice());
	}

}
