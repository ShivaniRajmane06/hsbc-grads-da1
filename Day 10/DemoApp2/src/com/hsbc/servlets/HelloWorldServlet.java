package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.hsbc.model.User;

public class HelloWorldServlet extends HttpServlet{
	
	@Override
	public void init() {
		System.out.println(" Inside the init method of the servlet");
	}

	@Override
	public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException{
		LocalDateTime currentDate = LocalDateTime.now();
		
		User user = new User("Shivani",22);
		
		if(httpServletRequest.getParameter("flag").equals("session")) {
			HttpSession session = httpServletRequest.getSession();
			session.setAttribute("user",user);
			session.setAttribute("userName", "Mayuri");
			
			Cookie cookie = new Cookie("place", "bangalore");
			cookie.setMaxAge(20);
			Cookie preference = new Cookie("food", "north-indian");
			httpServletResponse.addCookie(cookie);
			httpServletResponse.addCookie(preference);

		}
		
		/*
		 * PrintWriter out = httpServletResponse.getWriter();
		 * 
		 * out.write("<h1> The current Date is: " +currentDate.toString()+" </h1>");
		 */
	}
}
