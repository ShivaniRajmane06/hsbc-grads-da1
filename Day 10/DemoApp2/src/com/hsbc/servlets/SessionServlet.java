package com.hsbc.servlets;

import java.util.Enumeration;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SessionServlet extends HttpServlet{
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) {
		HttpSession session = req.getSession();
		Enumeration<String> attributeNames = session.getAttributeNames();
		while(attributeNames.hasMoreElements()) {
			System.out.println("Attribute Name: " +attributeNames.nextElement());
		}
		
		System.out.println(" Printing all the cookies ");
		Cookie[] cookies = req.getCookies();
		
		
		for ( Cookie c: cookies) {
			System.out.printf("Cookie name %s Cooke Value %s %n", c.getName(), c.getValue());
		}
	}


}
