package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.model.Items;
import com.hsbc.service.ItemService;

public class ListItemServlet extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		ItemService itemService = ItemService.getItemService();
		List<Items> items = itemService.fetchItems();
		
		req.setAttribute("items", items);
		RequestDispatcher rd = req.getRequestDispatcher("list.jsp");
		rd.forward(req, res);

		
		/*
		 * PrintWriter pw = res.getWriter();
		 * pw.write("<h1>Following is the List of Items in the Inventory : </h1><br>");
		 * 
		 * pw.write("<table>"); pw.write("<tr>");
		 * pw.write("<th>Item ID</th><th>Item Name</th><th>Price</th>");
		 * pw.write("</tr>");
		 * 
		 * for(Items item : items) { pw.write("<tr>"); pw.write("<td>" +
		 * item.getItemId() + "</td>"); pw.write("<td>" + item.getItemName() + "</td>");
		 * pw.write("<td>" + item.getPrice() + "</td>"); pw.write("</tr>"); }
		 * pw.write("</table>");
		 */
	}

}

/*
 * ItemService itemService = ItemService.getItemService(); List<Item> items =
 * itemService.fetchItems(); req.setAttribute("items", items); RequestDispatcher
 * rd = req.getRequestDispatcher("list.jsp"); rd.forward(req, res);
 */
