package com.hsbc.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.model.Items;
import com.hsbc.service.ItemService;

public class ItemServlet extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String itemName = request.getParameter("name");
		double itemPrice = Double.parseDouble(request.getParameter("price"));
		
		Items item = new Items(itemName, itemPrice);
		ItemService itemService = ItemService.getItemService();
		itemService.createItem(item);
		response.sendRedirect("listItems");
	}
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ItemService itemService = ItemService.getItemService();
		List<Items> items = itemService.fetchItems();
		
		for(Items item: items) {
			System.out.println(item);
		}

	}

	

}
