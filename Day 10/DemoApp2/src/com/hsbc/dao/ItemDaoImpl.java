package com.hsbc.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hsbc.model.Items;

public class ItemDaoImpl implements ItemDao{
	
	private static final String SELECT_QUERY = "select * from items";
	private static final String INSERT_QUERY = "insert into items (itemName, price) values(?, ?)";
	private static final String DELETE_QUERY = "delete from items where itemName = ?";
	private static final String SELECT_ITEM_QUERY = "select * from items where itemId = ?";
	
	static Connection getConnection() {
		try {
			Class.forName("org.apache.derby.jdbc.ClientDriver");
			Connection connection = DriverManager.getConnection("jdbc:derby://localhost:1527/ItemDatabase", "shivani", "shivani");
			return connection;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Items saveItem(Items item) {
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY);
			preparedStatement.setString(1, item.getItemName());
			preparedStatement.setDouble(2, item.getPrice());
			int rowsUpdated = preparedStatement.executeUpdate();
			if(rowsUpdated > 0)
				return item;
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Item not inserted");
		return item;
	}

	@Override
	public List<Items> fetchItems() {
		List<Items> items = new ArrayList<>();
		Connection connection = getConnection();
		
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(SELECT_QUERY);
			while(rs.next()) {
				items.add(new Items(rs.getLong("itemId"), rs.getString("itemName"), rs.getDouble("price")));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return items;
	}
	
	public void deleteItem(String itemName) {
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY);
			preparedStatement.setString(1, itemName);
			int rowsUpdated = preparedStatement.executeUpdate();
			if(rowsUpdated > 0)
				return;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("Item Not Deleted.");
		
		
	}

	@Override
	public Items fetchItem(long itemId) {
		Connection connection = getConnection();
		
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ITEM_QUERY);
			preparedStatement.setLong(1, itemId);
			ResultSet rs = preparedStatement.executeQuery();
			return new Items(rs.getLong("itemId"), rs.getString("itemName"), rs.getDouble("price"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}


/*
 * create table items(itemId bigint not null generated always as identity (start
 * with 0, increment by 1), itemName varchar(10) not null, price decimal(10,2),
 * constraint primary_key primary key(itemId));
 */
