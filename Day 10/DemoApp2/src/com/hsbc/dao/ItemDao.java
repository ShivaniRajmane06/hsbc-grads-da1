package com.hsbc.dao;

import com.hsbc.model.*;
import java.util.*;

public interface ItemDao {
	
	Items saveItem(Items item);
	List<Items> fetchItems();
	Items fetchItem(long itemId);
	void deleteItem(String itemName);
	
	static ItemDao getInstance() {
		return new ItemDaoImpl();
	}

}
