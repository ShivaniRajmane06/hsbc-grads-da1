<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*,com.hsbc.model.Items" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Listing all the Items</title>
<style>
  table {
    border: 1px solid red;
  }
  td, tr {
    border: 1px solid red;
  }
</style>
</head>
<body>

       <%
         List<Items> items = (List)request.getAttribute("items");
        %>

  <table>
    <thead>
      <tr>
       <th>Id</th>
       <th>Name</th>
       <th>Price</th>
      </tr> 
    </thead>
    <tbody>
      <%
         for ( Items item: items) {
      %>
          <tr>
             <td>
              <%= item.getItemId() %>
             </td>
          
             <td>
              <%= item.getItemName() %>
             </td>
             <td>
              <%= item.getPrice() %>
             </td>
          </tr>
      <% 
         }
       %>
    </tbody>
  </table>

</body>
</html>
