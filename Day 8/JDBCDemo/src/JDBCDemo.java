import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.derby.client.am.SqlException;

public class JDBCDemo {
	private static final String INSERT_QUERY = "INSERT INTO ITEMS (NAME, PRICE) VALUES ('DRESS', 1000)";
	
	public static void main(String[] args) {
		
		
		
		try(
				Connection connection = DriverManager.getConnection("jdbc:derby://localhost:1527/mydb", "shivani", "shivani");
				Statement statement = connection.createStatement();
			){
			
			int count = statement.executeUpdate(INSERT_QUERY);
			System.out.println("Value : " + count);
			
	
			
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}

}
