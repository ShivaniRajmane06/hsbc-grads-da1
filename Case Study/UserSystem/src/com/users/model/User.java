package com.users.model;

import java.time.LocalDate;

public class User {
	
	private long userId;
	private String userName;
	private String emailId;
	private String password;
	private LocalDate dob;
	
	public User (long userId, String userName, String emailId, String password, LocalDate dob) {
		this.userId = userId;
		this.userName = userName;
		this.emailId = emailId;
		this.password = password;
		this.dob = dob;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDate getDob() {
		return dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	public long getUserId() {
		return userId;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", emailId=" + emailId + ", password=" + password
				+ ", dob=" + dob + "]";
	}
	
	

}

/*
 * create table users(userid bigint not null generated always as identity (start
 * with 1, increment by 1), username varchar(10), emailid varchar(20), password
 * varchar(30), dob varchar(30), constraint primary_key primary key(userid));
 */