package com.users.dao;

import com.users.model.*;
import java.util.*;

public interface UserDao {
	
	User saveUser(User user);
	List<User> fetchUsers();
	void deleteUser(String emailId);
	User updateUser(String emailId);
	
	
	static UserDao getInstance() {
		return new UserDaoImpl();
	}
}
