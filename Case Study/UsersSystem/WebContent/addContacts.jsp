<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
			
	<title>Add Contacts</title>
	</head>
<body>
	<%
		String emailid = (String)session.getAttribute("user");
		if(emailid == null){
			response.sendRedirect("login.jsp");
		}
		String name = (String)session.getAttribute("name");
	%>
	
		<div class="container">
			<div class="container mt-2">
			    <nav class="navbar navbar-expand-lg sticky-top navbar-dark bg-dark" >
			  		<a class="navbar-brand" href="#">Contact Manager</a>
				
					<div class="collapse navbar-collapse" id="navbarTogglerDemo03">
					    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					    	
						    <li class="nav-item">
						        <a class="nav-link" href="viewContacts.jsp"> View All Contacts <span class="sr-only">(current)</span></a>
						    </li>
						    
						    <li class="nav-item active">
					        	<a class="nav-link" href="addContacts.jsp"> Add Contacts </a>
						    </li>
						    
						    <li class="nav-item">
						        <a class="nav-link" href="deleteProfile.jsp"> Delete Profile</a>
						    </li>
						 </ul>
						 <ul class = "navbar-nav">
						    <li class="nav-item" style="float: right;color:white">
						    	Welcome <%=name %> !
						    </li>
						 </ul>
				  	</div>
			  	</nav>
			</div>
			<!-- End of nav Bar -->
			
			
			<div class="container" style="margin-top : 100px">
				<div class="row">
					<div class="col-md-4"></div> 
					<div>
						
						<form method="post" action="addContact">
							
							Enter Contact Name :   <input type="text" name = "contactName"> <br>
							Enter Contact Number : <input type="text" name = "contactNumber"> <br>
							<center><input type="submit" value="Add Contact"></center>
						</form>
					
					</div>
				</div>
			</div>	
		</div>
</body>
</html>