<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="/CSS/login.css" rel="stylesheet">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="header.jsp" %>
	
	<div class="container mt-2">
		<div class="container" style="margin-top:100px">
			<div class=row>
			<div class="col-md-4"></div>
				<div class=col-md-3>
				    <form method="post" action="login">
				      <input type="text" id="emailid"  name="emailid" placeholder="login" style="width:100%">
				      <input type="password" id="password"  name="password" placeholder="password" style="width:100%">
				      <center><input type="submit" value="Log In"></center>
				    </form>
				
				    <!-- Register -->
				    
				    <div id="formFooter">
				      <center><a class="underlineHover" href="register.jsp">Not a User ? Register</a></center>
				    </div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>