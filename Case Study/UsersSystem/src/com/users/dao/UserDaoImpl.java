package com.users.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import com.users.model.Contact;
import com.users.model.User;

public class UserDaoImpl implements UserDao {

	private static final String INSERT_QUERY = "insert into users (username, emailid, password, phonenumber, dob) values (?,?,?,?,?)";
	private static final String SELECT_QUERY = "select * from users";
	private static final String DELETE_QUERY = "delete from users where emailid = ?";
	private static final String SELECT_BY_ID = "select * from users where emailid = ?";
	
	private static final String INSERT_CONTACT = "insert into contacts (contactname, contactnumber, userid) values(?,?,?)";
	private static final String SELECT_CONTACTS = "select * from contacts";
	private static final String SELECT_CONTACT_BY_ID = "select * from contacts where userid = ?";
	
	
	
	static Connection getConnection() {
		try {
			Class.forName("org.apache.derby.jdbc.ClientDriver");
			Connection connection = DriverManager.getConnection("jdbc:derby://localhost:1527/UserDatabase", "shivani", "shivani");
			return connection;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public User saveUser(User user) {
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY);
			
			preparedStatement.setString(1, user.getUserName());
			preparedStatement.setString(2, user.getEmailId());
			preparedStatement.setString(3, user.getPassword());
			preparedStatement.setLong(4,user.getPhoneNumber());
			preparedStatement.setString(5, user.getDob());
			
			int rowsInserted = preparedStatement.executeUpdate();
			
			if(rowsInserted > 0) {
				System.out.println("Item Inserted");
				return user;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<User> fetchUsers() {
		Connection connection = getConnection();
		try {
			Statement statement = connection.createStatement();
			List<User> users = new ArrayList<>();
			ResultSet rs = statement.executeQuery(SELECT_QUERY);
			
			while(rs.next()) {
				users.add(new User(rs.getLong("userid"), rs.getString("username"), rs.getString("emailid"), rs.getString("password"),rs.getLong("phonenumber"), rs.getString("dob")));
			}
			return users;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void deleteUser(String emailId) {
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY);
			preparedStatement.setString(1, emailId);
			int rowsDeleted = preparedStatement.executeUpdate();
			if(rowsDeleted > 0)
				return;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("User not deleted");
	}

	@Override
	public User updateUser(String emailId) {
		
		return null;
	}

	@Override
	public User fetchUserById(String emailId) {
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID);
			preparedStatement.setString(1, emailId);
			ResultSet rs = preparedStatement.executeQuery();
			if(rs.next()) {
				return new User(rs.getLong("userid"), rs.getString("username"), rs.getString("emailid"), rs.getString("password"), rs.getLong("phonenumber"), rs.getString("dob"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Contact saveContact(Contact contact) {
		
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY);
			
			preparedStatement.setString(1, contact.getContactName());
			preparedStatement.setLong(2, contact.getContactNumber());
			preparedStatement.setLong(3, contact.getUserId());
			
			int rowsInserted = preparedStatement.executeUpdate();
			
			if(rowsInserted > 0) {
				System.out.println("Contact Inserted");
				return contact;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Contact not inserted");
		return null;
	}

	@Override
	public List<Contact> fetchContacts(String userId) {
		Connection connection = getConnection();
		
		try {
			PreparedStatement statement = connection.prepareStatement("select userid from users where emailid = ?");
			statement.setString(1, userId);
			
			ResultSet rs = statement.executeQuery();
			long id = rs.getLong("userid");
			
			rs = statement.executeQuery(SELECT_CONTACT_BY_ID);
			List<Contact> contacts = new ArrayList<>();
			
			while(rs.next()) {
				if(rs.getLong("userid") == id)
					contacts.add(new Contact(rs.getLong("contactid"), rs.getLong("userid"),rs.getString("contactname"),rs.getLong("contactnumber")));
			}
			
			return contacts;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void deleteContact(String contactName) {
		
		
	}

	@Override
	public Contact updateContact(String contactName) {
		
		return null;
	}

	@Override
	public Contact fetchContactByName(String contactName) {
		
		return null;
	}

}
