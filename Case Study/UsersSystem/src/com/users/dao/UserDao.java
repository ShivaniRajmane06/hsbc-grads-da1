package com.users.dao;

import com.users.model.*;
import java.util.*;

public interface UserDao {
	
	User saveUser(User user);
	List<User> fetchUsers();
	void deleteUser(String emailId);
	User updateUser(String emailId);
	User fetchUserById(String emailId);
	
	Contact saveContact(Contact contact);
	List<Contact> fetchContacts(String userId);
	void deleteContact(String contactName);
	Contact updateContact(String contactName);
	Contact fetchContactByName(String contactName);
	
	static UserDao getInstance() {
		return new UserDaoImpl();
	}
	
}
