package com.users.model;

public class Contact {
	private long userId;
	private long contactId;
	private String contactName;
	private long contactNumber;
	
	public Contact(long contactId, long userId, String contactName, long contactNumber) {
		this.contactId = contactId;
		this.contactName = contactName;
		this.contactNumber = contactNumber;
		this.userId = userId;
	}
	
	public Contact(long userId, String contactName, long contactNumber) {
		this.contactName = contactName;
		this.contactNumber = contactNumber;
		this.userId = userId;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(long contactNumber) {
		this.contactNumber = contactNumber;
	}

	public long getUserId() {
		return userId;
	}

	public long getContactId() {
		return contactId;
	}

	@Override
	public String toString() {
		return "Contact [userId=" + userId + ", contactId=" + contactId + ", contactName=" + contactName
				+ ", contactNumber=" + contactNumber + "]";
	}
	
	
}
