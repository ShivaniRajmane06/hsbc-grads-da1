package com.users.model;

public class User {
	
	private long userId;
	private String userName;
	private String emailId;
	private String password;
	private String dob;
	private long phoneNumber;
	
	public User (long userId, String userName, String emailId, String password, long phoneNumber, String dob) {
		this.userId = userId;
		this.userName = userName;
		this.emailId = emailId;
		this.password = password;
		this.dob = dob;
		this.phoneNumber = phoneNumber;
	}

	public User(String name, String emailId, String password, long mobile, String dob) {
		this.userName = name;
		this.emailId = emailId;
		this.password = password;
		this.dob = dob;
		this.phoneNumber = mobile;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDob() {
		
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public long getUserId() {
		return userId;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", emailId=" + emailId + ", password=" + password
				+ ", dob=" + dob + ", phoneNumber=" + phoneNumber + "]";
	}

	
	
	

}


/*
 * create table users(userid bigint not null generated always as identity (start
 * with 1, increment by 1), username varchar(10), emailid varchar(20), password
 * varchar(30),phonenumber bigint, dob varchar(30), constraint primary_key
 * primary key(userid));
 */



/*
 * CREATE TABLE contacts(contactID bigint NOT NULL generated always as
 * identity(start with 100,increment by 1),contactNAME VARCHAR(25) unique NOT
 * NULL,contactNumber bigint,userid bigint not null CONSTRAINT city_foreign_key
 * REFERENCES users ON DELETE CASCADE ON UPDATE RESTRICT ,CONSTRAINT
 * contact_primary_key Primary key (contactID));
 */
 
 