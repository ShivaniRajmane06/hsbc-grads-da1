package com.users.service;

import java.util.*;
import com.users.model.*;
import com.users.dao.*;

public class UserServiceImpl implements UserService {

	private UserDao userDao = UserDao.getInstance();
	@Override
	public User saveUser(User user) {
		
		return userDao.saveUser(user);
	}

	@Override
	public List<User> fetchUsers() {
		return userDao.fetchUsers();
	}

	@Override
	public void deleteUser(String emailId) {
		userDao.deleteUser(emailId);
	}

	@Override
	public User updateUser(String emailId) {
		return userDao.updateUser(emailId);
	}

	@Override
	public User fetchUserById(String emailId) {
		return userDao.fetchUserById(emailId);
	}

	@Override
	public Contact saveContact(Contact contact) {
		
		return userDao.saveContact(contact);
	}

	@Override
	public List<Contact> fetchContacts(String userId) {
		
		return userDao.fetchContacts(userId);
	}

	@Override
	public void deleteContact(String contactName) {
		
		userDao.deleteContact(contactName);
	}

	@Override
	public Contact updateContact(String contactName) {
		
		return userDao.updateContact(contactName);
	}

	@Override
	public Contact fetchContactByName(String contactName) {
		
		return userDao.fetchContactByName(contactName);
	}

}
