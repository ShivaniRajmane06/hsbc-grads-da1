package com.users.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.users.model.User;
import com.users.service.UserService;

public class LoginServlet extends HttpServlet{
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String emailId = req.getParameter("emailid");
		String password = req.getParameter("password");
		
		UserService userService = UserService.getInstance();
		User user = userService.fetchUserById(emailId);
		HttpSession session = req.getSession();
		if(user != null && user.getPassword().equals(password)) {
			System.out.println("Hi");
			session.setAttribute("name", ""+user.getUserName());
			session.setAttribute("user", emailId);
			res.sendRedirect("index.jsp");
		}else {
			session.setAttribute("user", null);
			
			PrintWriter out = res.getWriter();

			out.println("<script type=\"text/javascript\">");
		       out.println("alert('Login failed :(. User or password incorrect');");
		       out.println("</script>");
		       //res.sendRedirect("index.jsp");
			
		}
	}

}
