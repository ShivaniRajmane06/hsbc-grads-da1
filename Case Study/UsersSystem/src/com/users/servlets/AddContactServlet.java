package com.users.servlets;

import java.io.IOException;
import com.users.model.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.users.model.User;
import com.users.service.UserService;

public class AddContactServlet extends HttpServlet{
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		HttpSession session = req.getSession();
		String emailId = (String)session.getAttribute("user");
		
		String name = req.getParameter("contactName");
		long mobile = Long.parseLong(req.getParameter("contactNumber"));
		
		UserService userService = UserService.getInstance();
		User user = userService.fetchUserById(emailId);
		Contact contact = userService.saveContact(new Contact(user.getUserId(), name, mobile));
		if(user != null) {
			res.sendRedirect("login.jsp");
		}
	}

}
