package com.users.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.users.service.UserService;
import com.users.model.*;

public class RegisterServlet extends HttpServlet{
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String name = req.getParameter("name");
		String emailId = req.getParameter("emailid");
		String password = req.getParameter("password");
		long mobile = Long.parseLong(req.getParameter("number"));
		String dob = req.getParameter("dob");
		//System.out.println(name+emailId+password+mobile+dob);
		
		UserService userService = UserService.getInstance();
		User user = userService.saveUser(new User(name,emailId,password,mobile,dob));
		if(user != null) {
			res.sendRedirect("login.jsp");
		}
	}

}
