package com.employeemanagementsystem.model;

public class Employee implements Comparable<Employee>{
	
	private String employeeName;
	
	private long employeeId;
	
	private int employeeAge;
	
	private double employeeSalary;
	
	private int remainingLeaves;
	
	private static long counter = 1000;
	
	public Employee(String employeeName, double employeeSalary, int employeeAge) {
		this.employeeName = employeeName;
		this.employeeAge = employeeAge;
		this.employeeSalary = employeeSalary;
		this.remainingLeaves = 40;
		this.employeeId = ++counter;
	}

	public int getRemainingLeaves() {
		return remainingLeaves;
	}

	public void setRemainingLeaves(int remainingLeaves) {
		this.remainingLeaves = remainingLeaves;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public int getEmployeeAge() {
		return employeeAge;
	}

	public double getEmployeeSalary() {
		return employeeSalary;
	}

	@Override
	public String toString() {
		return "Employee [employeeName=" + employeeName + ", employeeId=" + employeeId + ", employeeAge=" + employeeAge
				+ ", employeeSalary=" + employeeSalary + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (employeeId ^ (employeeId >>> 32));
		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (employeeId != other.employeeId)
			return false;
		if (employeeName == null) {
			if (other.employeeName != null)
				return false;
		} else if (!employeeName.equals(other.employeeName))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Employee employee) {
	 	 
		return -1 * employee.employeeName.compareTo(this.employeeName);
	}


}

	
