package com.employeemanagementsystem.util;
import com.employeemanagementsystem.dao.*;
import com.employeemanagementsystem.service.*;

public class EmployeeServiceFactory {
	
	public static EmployeeManagementSystemService getInstance(EmployeeManagementSystemDao dao) {
		return new EmployeeManagementSystemServiceImpl(dao);
	}
}
