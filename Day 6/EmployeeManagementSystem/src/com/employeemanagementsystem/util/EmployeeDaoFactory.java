package com.employeemanagementsystem.util;

import com.employeemanagementsystem.dao.*;

public final class EmployeeDaoFactory {
	
	public static EmployeeManagementSystemDao getEmployeeDao(int value) {
		
		if(value == 1) {
			return new ArrayListBackedEmployeeManagementSystemDaoImpl();
		} else {
			return new ArrayBackedEmployeeManagementSystemDaoImpl();
		}
	}
}
