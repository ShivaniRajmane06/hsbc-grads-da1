package com.employeemanagementsystem.service;

import com.employeemanagementsystem.exception.*;
import com.employeemanagementsystem.model.Employee;

import java.util.*;

public interface EmployeeManagementSystemService {
	
	public Employee createEmployee(String employeeName, double employeeSalary, int employeeAge);
	
	public void deleteEmployee(long employeeId);
	
	public Collection<Employee> fetchAllEmployees();
	
	public Collection<Employee> fetchEmployeeByName(String employeeName);
	
	public Employee fetchEmployeeById(long employeeId) throws EmployeeNotFoundException;
	
	public Employee updateEmployee(long empoyeeId, Employee employee);
	
	public int applyForLeave(long employeeId, int noOfDays) throws EmployeeNotFoundException, NotEligibleForLeaveException;
}
