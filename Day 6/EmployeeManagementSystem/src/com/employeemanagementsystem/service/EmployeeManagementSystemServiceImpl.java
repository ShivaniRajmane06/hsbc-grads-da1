package com.employeemanagementsystem.service;

import java.util.Collection;

import com.employeemanagementsystem.dao.*;
import com.employeemanagementsystem.exception.*;
import com.employeemanagementsystem.model.Employee;

public class EmployeeManagementSystemServiceImpl implements EmployeeManagementSystemService{

	private EmployeeManagementSystemDao dao;
	
	public EmployeeManagementSystemServiceImpl(EmployeeManagementSystemDao dao) {
		this.dao = dao;
	}
	
	@Override
	public Employee createEmployee(String employeeName, double employeeSalary, int employeeAge) {
		
		Employee employee = new Employee(employeeName, employeeSalary, employeeAge);
		return this.dao.saveEmployee(employee);
	}

	@Override
	public void deleteEmployee(long employeeId){
		this.dao.deleteEmployee(employeeId);
		
	}

	@Override
	public Collection<Employee> fetchAllEmployees() {
		return this.dao.fetchAllEmployees();
	}

	@Override
	public Collection<Employee> fetchEmployeeByName(String employeeName) {
		
		return this.dao.fetchEmployeeByName(employeeName);
	}

	@Override
	public Employee fetchEmployeeById(long employeeId) throws EmployeeNotFoundException{
		return this.dao.fetchEmployeeById(employeeId);
	}

	@Override
	public Employee updateEmployee(long employeeId, Employee employee) {
		
		return this.dao.updateEmployeeDetails(employeeId, employee);
	}

	@Override
	public int applyForLeave(long employeeId, int noOfDays) throws EmployeeNotFoundException, NotEligibleForLeaveException{
		Employee employee = this.dao.fetchEmployeeById(employeeId);
		int remainingLeaves = employee.getRemainingLeaves();
		
		if(remainingLeaves >= noOfDays && noOfDays <= 10 && employee != null) {
			employee.setRemainingLeaves(remainingLeaves - noOfDays);
			updateEmployee(employeeId, employee);
			return employee.getRemainingLeaves();
		}
		else
			throw new NotEligibleForLeaveException("You are not eligible for taking");
	}

}
