package com.employeemanagementsystem.client;

import java.util.Scanner;

import com.employeemanagementsystem.controller.*;
import com.employeemanagementsystem.dao.*;
import com.employeemanagementsystem.exception.*;
import com.employeemanagementsystem.model.*;
import com.employeemanagementsystem.service.*;
import com.employeemanagementsystem.util.*;

public class EmployeeManagementSystemClient {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please enter your option : ");
		System.out.println("1. Array Backed");
		System.out.println("1. List Backed");
		System.out.println("1. Set Backed");
		
		int choice = sc.nextInt();
		
		EmployeeManagementSystemDao dao = EmployeeDaoFactory.getEmployeeDao(choice);
		EmployeeManagementSystemService service = EmployeeServiceFactory.getInstance(dao);
		
		EmployeeManagementSystemController controller = new EmployeeManagementSystemController(service);
		
		Employee shivani = controller.createEmployee("Shivani Rajmane", 54000, 22);
		Employee mayuri = controller.createEmployee("Mayuri Nanaware", 55000, 22);
		Employee rashmi = controller.createEmployee("Rashmi Shewale", 50000, 21);
		Employee sanved = controller.createEmployee("Sanved Joshi", 60000, 25);
		
		try {
			System.out.println("5 days Leave Application for Shivani.Remaining Leaves : " + controller.applyForLeaves(shivani.getEmployeeId(), 5));
		}catch(EmployeeNotFoundException | NotEligibleForLeaveException e) {
			System.out.println(e.getMessage());
		}
	}
}
