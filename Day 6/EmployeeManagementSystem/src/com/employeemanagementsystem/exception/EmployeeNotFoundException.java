package com.employeemanagementsystem.exception;

public class EmployeeNotFoundException extends Exception{
	
	public EmployeeNotFoundException(String message) {
		super(message);
	}
	
	public String getMessage() {
		return super.getMessage();
	}

}
