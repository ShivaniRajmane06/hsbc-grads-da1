package com.employeemanagementsystem.exception;

public class NotEligibleForLeaveException extends Exception{
	
	public NotEligibleForLeaveException(String message) {
		super(message);
	}
	
	public String getMessage() {
		return super.getMessage();
	}

}
