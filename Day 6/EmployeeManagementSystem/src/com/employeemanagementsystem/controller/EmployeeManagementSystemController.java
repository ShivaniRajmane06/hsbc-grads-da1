package com.employeemanagementsystem.controller;

import com.employeemanagementsystem.exception.*;
import com.employeemanagementsystem.model.*;
import com.employeemanagementsystem.service.*;

import java.util.*;



public class EmployeeManagementSystemController {
	private EmployeeManagementSystemService employeeManagementSystemService;
	
	public EmployeeManagementSystemController(EmployeeManagementSystemService service) {
		this.employeeManagementSystemService = service;
	}
	
	public Employee createEmployee(String employeeName, double employeeSalary, int employeeAge) {
		return this.employeeManagementSystemService.createEmployee(employeeName, employeeSalary, employeeAge);
	}
	
	public void deleteEmployee(long employeeId) {
		this.employeeManagementSystemService.deleteEmployee(employeeId);
	}
	
	public Collection<Employee> fetchAllEmployees() {
		return employeeManagementSystemService.fetchAllEmployees();
	}
	
	public Employee fetchEmployeeById(long employeeId)throws EmployeeNotFoundException {
		return this.employeeManagementSystemService.fetchEmployeeById(employeeId);
	}
	
	public Collection<Employee> fetchEmployeeByName(String employeeName) {
		return this.employeeManagementSystemService.fetchEmployeeByName(employeeName);
	}
	
	public int applyForLeaves(long employeeId, int noOfDays) throws EmployeeNotFoundException, NotEligibleForLeaveException {
		return this.employeeManagementSystemService.applyForLeave(employeeId, noOfDays);
	}
}
