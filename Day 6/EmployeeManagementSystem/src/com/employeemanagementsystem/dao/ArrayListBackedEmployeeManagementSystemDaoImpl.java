package com.employeemanagementsystem.dao;

import java.util.*;

import com.employeemanagementsystem.exception.*;
import com.employeemanagementsystem.model.Employee;

public class ArrayListBackedEmployeeManagementSystemDaoImpl implements EmployeeManagementSystemDao{

	private List<Employee> employees = new ArrayList<>();
	
	@Override
	public Employee saveEmployee(Employee employee) {
		this.employees.add(employee);
		return employee;
	}

	@Override
	public Employee updateEmployeeDetails(long employeeId, Employee employee) {
		for(Employee e : employees) {
			if(e.getEmployeeId() == employeeId) {
				e = employee;
			}
		}
		return employee;
	}

	@Override
	public void deleteEmployee(long employeeId){
		
		for(Employee e : employees) {
			if(e.getEmployeeId() == employeeId) {
				this.employees.remove(e);
			}
		}
		
	}

	@Override
	public Collection<Employee> fetchAllEmployees() {
		return employees;
	}

	@Override
	public Employee fetchEmployeeById(long employeeId) throws EmployeeNotFoundException{
		for(Employee e : employees) {
			if(e.getEmployeeId() == employeeId) {
				return e;
			}
		}
		throw new EmployeeNotFoundException("Employee not found");
	}

	@Override
	public Collection<Employee> fetchEmployeeByName(String employeeName) {
		List<Employee> employeeList = new ArrayList<>();
		for(Employee e : employees) {
			if(e.getEmployeeName() == employeeName) {
				employeeList.add(e);
			}
		}
		return employeeList;
	}

}
