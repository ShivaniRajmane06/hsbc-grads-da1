package com.employeemanagementsystem.dao;

import java.util.*;

import com.employeemanagementsystem.exception.EmployeeNotFoundException;
import com.employeemanagementsystem.model.Employee;

public class ArrayBackedEmployeeManagementSystemDaoImpl implements EmployeeManagementSystemDao{

	private Employee[] employees = new Employee[10];
	private static int counter = 0;
	
	@Override
	public Employee saveEmployee(Employee employee) {
		this.employees[counter++] = employee;
		return employee;
	}

	@Override
	public Employee updateEmployeeDetails(long employeeId, Employee employee) {
		for(Employee e : employees) {
			if(e.getEmployeeId() == employeeId && e != null) {
				e = employee;
			}
		}
		return employee;
	}

	@Override
	public void deleteEmployee(long employeeId) {
		
		for(int index = 0; index < employees.length; index++) {
			if(employees[index].getEmployeeId() == employeeId) {
				employees[index] = null;
			}
		}
		
	}

	@Override
	public List<Employee> fetchAllEmployees() {
		return Arrays.asList(employees);
	}

	@Override
	public Employee fetchEmployeeById(long employeeId) throws EmployeeNotFoundException{
		for(Employee e : employees) {
			if(e.getEmployeeId() == employeeId) {
				return e;
			}
		}
		throw new EmployeeNotFoundException("Employee not found");
	}

	@Override
	public List<Employee> fetchEmployeeByName(String employeeName) {
		Employee[] employeeList = new Employee[10];
		int empCounter = 0;
		for(Employee e : employees) {
			if(e.getEmployeeName() == employeeName) {
				employeeList[empCounter++] = e;
			}
		}
		return Arrays.asList(employeeList);
	}

}
