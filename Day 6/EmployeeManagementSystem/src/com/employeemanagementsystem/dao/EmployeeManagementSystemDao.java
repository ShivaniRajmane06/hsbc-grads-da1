package com.employeemanagementsystem.dao;
import com.employeemanagementsystem.exception.*;
import com.employeemanagementsystem.model.*;

import java.util.*;

public interface EmployeeManagementSystemDao {
	
	Employee saveEmployee(Employee employee);
	
	Employee updateEmployeeDetails(long employeeId, Employee employee);
	
	void deleteEmployee(long employeeId);
	
	Collection<Employee> fetchAllEmployees();
	
	Employee fetchEmployeeById(long employeeId) throws EmployeeNotFoundException;
	
	Collection<Employee> fetchEmployeeByName(String employeeName);
	
	
}
