import java.io.*;

public class FileOperations {

	static void readWriteFile(File sourceFile, File destinationFile) {
		try(
				BufferedReader fileReader = new BufferedReader(new FileReader(sourceFile));
				BufferedWriter fileWriter = new BufferedWriter(new FileWriter(destinationFile));
		) {			
			boolean endOfFile = false;
			while(!endOfFile) {
				String line = fileReader.readLine();
				if(line != null) {
					fileWriter.append(line + "\n");
					//System.out.print(c);
				} else {
					endOfFile = true;
				}
			}
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		File sourceFile = new File("/home/shivani/Details.txt");
		File destFile = new File("/home/shivani/Detailss.txt");
		readWriteFile(sourceFile, destFile);
		
	}

}
