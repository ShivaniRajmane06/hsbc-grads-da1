import java.util.*;

public class SetDemo {

	public static void main(String[] args) {
		
		Set<Integer> set = new HashSet();
		set.add(6);
		set.add(8);
		set.add(1998);
		set.add(22);
		
		Iterator<Integer> it = set.iterator();
		
		while(it.hasNext())
			System.out.print(it.next() + "\t");
		
		System.out.println("\nTotal number of elements in the set are : " + set.size());
		
		Set set1 = new HashSet();
		set1.add(set);
		set1.add("Shivani");
		set1.add(set1);
		
		System.out.println("Number of elements in the set1 are : " + set1.size());
		
		Iterator it1 = set1.iterator();
		
		while(it1.hasNext())
			System.out.print(it1.next() + "\t");
	}

}
