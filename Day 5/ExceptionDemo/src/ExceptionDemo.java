class Base {
	
}

class Derived extends Base{
	
}

class Derived1 extends Base {
	
}


public class ExceptionDemo {

	public static void main(String[] args) {
		
		int a = 10, b = 0;
		int result;
		int array[] = {11, 22, 33, 44};
		
		try {
			result = a / 10;
			System.out.println("Array last element : " + array[5]);
			Base base = new Derived();
			Derived derived = (Derived)base;
			
		}catch(ArithmeticException e) {
			System.out.println("Cannot Divide by zero. " + e);
		}catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("Access array elements within range. " + e);
		}catch(ClassCastException e) {
			System.out.println("DownCasting not allowed " + e);
		}catch(Exception e) {
			System.out.println("Generic Exception " + e);
		}catch(Throwable t) {
			System.out.println(t);
		}

	}

}
