public class CurrentAccountRegister {
	public static CurrentAccount[] currentAccountList = new CurrentAccount[]{
		new CurrentAccount("Shivani Rajmane", "HSBC", 100000, "27CWHDGSH12H1 8"),
		new CurrentAccount("Shivani Rajmane", "Deutsche", 100000, new Address("Magarpatta", "Pune", 411041), "72CWHDGSH12H1 8")
	};

	public static CurrentAccount fetchAccountNumberByAccountId(long accountId) {
		for(CurrentAccount currentAccount : currentAccountList) {
			if(currentAccount.getAccountNumber() == accountId)
				return currentAccount;
		}
		return null;
	}
}