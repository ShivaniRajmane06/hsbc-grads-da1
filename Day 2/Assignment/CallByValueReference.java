public class CallByValueReference {

	static void addNumbers(int num1, int num2, int result) {
		result = num1 + num2;
		System.out.println("In method : " + num1 + ", " + num2 + ", " + result);
	}

	static void arrayMultiplication(int[] array) {

		for(int i = 0; i < 5; i++) {
			array[i] *= 5;
		}
	}

	public static void main(String[] args) {

		int arr[] = new int[5];

		for(int i = 0; i < args.length; i++) {
			arr[i] = Integer.parseInt(args[0]);
		}

		int num1 = 10;
		int num2 = 50;
		int result = 0;

		System.out.println("Numbers before calling Method : " + num1 + ", " + num2 + ", " + result);
		CallByValueReference.addNumbers(num1, num2, result);
		System.out.println("Numbers after calling Method : " + num1 + ", " + num2 + ", " + result);

		System.out.println("Array before calling method : ");
		for(int i = 0; i < 5; i++) {
			System.out.println(arr[i]);
		}

		CallByValueReference.arrayMultiplication(arr);	
		
		System.out.println("Array after calling method : ");
		for(int i = 0; i < 5; i++) {
			System.out.println(arr[i]);
		}	
	}
}