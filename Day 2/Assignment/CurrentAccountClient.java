public class CurrentAccountClient {

	public static void main(String[] args) {

		CurrentAccount shivani = new CurrentAccount("Shivani Rajmane", "HSBC", 100000, "27CWHDGSH12H1 8");
		System.out.println("Customer Name : " + shivani.getCustomerName());
		System.out.println("Business Name : " + shivani.getBusinessName());
		System.out.println("Account Balance : " + shivani.checkBalance());
		shivani.deposit(50000);
		System.out.println("Balance after deposit : " + shivani.checkBalance());
		shivani.withdraw(60000);
		System.out.println("Balance after withdraw : " + shivani.checkBalance());

		System.out.println("-----------------------------------------------------------------");

		Address address = new Address("Magarpatta", "Pune", 411041);
		CurrentAccount mayuri = new CurrentAccount("Shivani Rajmane", "Deutsche", 100000, address, "72CWHDGSH12H1 8");
		System.out.println("Customer Name : " + mayuri.getCustomerName());
		System.out.println("Business Name : " + mayuri.getBusinessName());
		System.out.println("Account Balance : " + mayuri.checkBalance());
		mayuri.deposit(500000);
		System.out.println("Balance after deposit : " + mayuri.checkBalance());
		mayuri.withdraw(6000);
		System.out.println("Balance after withdraw : " + mayuri.checkBalance());
		mayuri.transferAmount(50000, shivani.getAccountNumber());
		System.out.println("Balance after Transfer(Mayuri) : " + mayuri.checkBalance());
		System.out.println("Balance after deposit(Shivani) : " + shivani.checkBalance());

	}
}