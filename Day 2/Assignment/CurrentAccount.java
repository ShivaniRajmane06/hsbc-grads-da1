public class CurrentAccount {
	
	private static long accountNumberTracker = 10000;
	
	private long accountNumber;

	private double accountBalance;

	private String GSTNumber;

	private String customerName;

	private String businessName;

	private Address address;

	public CurrentAccount(String customerName, String businessName, String gstNumber) {
        this.customerName = customerName;
        this.businessName = businessName;
        this.accountBalance = 50000;
        this.accountNumber = ++ accountNumberTracker;
        this.GSTNumber = gstNumber;
    }

	public CurrentAccount(String customerName, String businessName, double initialAccountBalance, String gstNumber) {
        this.customerName = customerName;
        this.businessName = businessName;
        this.accountBalance = initialAccountBalance;
        this.accountNumber = ++ accountNumberTracker;
        this.GSTNumber = gstNumber;
    }

    public CurrentAccount(String customerName, String businessName, double initialAccountBalance, Address addess, String gstNumber) {
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.address = address;
        this.accountNumber = ++ accountNumberTracker;
        this.GSTNumber = gstNumber;
    }

    public double withdraw(double amount) {
        if (validateBalance(amount)) {
            this.accountBalance = this.accountBalance - amount;
            return this.accountBalance;
        }
        return this.accountBalance;
    }

    public boolean validateBalance(double amount) {
    	if(this.accountBalance >= (amount + 50000))
    		return true;
    	else 
    		return false;
    }

    public double checkBalance() {
        return this.accountBalance;
    }

    public double deposit(double amount) {
        this.accountBalance = accountBalance + amount;
        return accountBalance;
    }
    

    public double transferAmount(double amount, long accountId) {
    	CurrentAccount userAccount = CurrentAccountRegister.fetchAccountNumberByAccountId(accountId);
    	if(userAccount != null){
    		if (validateBalance(amount)) {
    			withdraw(amount);
    			userAccount.deposit(amount);
    		}
    	}
    	return this.accountBalance;
    }



    public void updateAddress(Address address) {

        this.address = address;
    }

    public String getCustomerName() {
        return this.customerName;
    }

   	public String getBusinessName() {
        return this.businessName;
    }

    public long getAccountNumber() {
    	return this.accountNumber;
    }
 
}