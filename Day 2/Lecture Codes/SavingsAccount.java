public class SavingsAccount{

	// instance variables
	private long accountNumber;

	private double accountBalance;

	private String customerName;

	private String street;

	private int zipCode;

	private String emailAddress;

	private String nominee;


	public long getAccountNumber() {
		return accountNumber;
	}


	public double withdraw(double amount) {
		if(this.accountBalance >= amount) {
			this.accountBalance = this.accountBalance - amount;
			return this.accountBalance;
		}
		return 0;
	}

	public double checkBalance() {
		return this.accountBalance;
	}

	public double deposit(double amount) {
		this.accountBalance = this.accountBalance + amount;
		return accountBalance;
	}

	public void updateAddress(String streetName, int zipCode, String city) {
		this.street = streetName;
		this.zipCode = zipCode;
		this.city = city;
	}

	public static void main(String[] args) {
		
		SavingsAccount shivani = new SavingsAccount();
		shivani.getAccountNumber();
		
	}
}