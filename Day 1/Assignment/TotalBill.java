/*
	Class to display the Total Bill amount after adding Tax.
*/

public class TotalBill {

	public static void main(String[] args) {
	
		float amount = Float.parseFloat(args[0]);
		
		switch(args[1]) {
		
			case "KA" :
				System.out.println("Your Total bill is : Rs." + (amount + (amount * 0.15)));
			break;

			case "TN" :
				System.out.println("Your Total bill is : Rs." + (amount + (amount * 0.18)));
				break;

			case "MH" :
				System.out.println("Your Total bill is : Rs." + (amount + (amount * 0.20)));
				break;

			default :
				System.out.println("Your Total bill is : Rs." + (amount + (amount * 0.12)));
				break;



		}
	}
}
