/*
	Class to determine the min and max values of all the numbers entered by the user.
*/

public class Numbers {

	public static void main(String[] args) {
	
		int min = 99999, max = -99999;

		for(int i = 0; i < args.length; i++) {
		
			if(Integer.parseInt(args[i]) > max)
				max = Integer.parseInt(args[i]);
			else if(Integer.parseInt(args[i]) < min)
				min = Integer.parseInt(args[i]);
		}

		System.out.println("Minimum : " + min);
		System.out.println("Maximum : " + max);
	}
}
