/* 
	Class to determine whether the value entered by user is a WeekDay or Weekend.
*/

public class DaysOfWeek {

	public static void main(String[] args) {
	
		for(int i = 0; i < args.length; i++) {
		
			switch(args[i]) {
			
				case "Monday" :
					System.out.println(args[i] + " : weekday");
					break;
					
				case "Tuesday" :
					System.out.println(args[i] + ": weekday");
					break;
					
				case "Wednesday" :
					System.out.println(args[i] + ": weekday");
					break;
					
				case "Thursday" :
					System.out.println(args[i] + ": weekday");
					break;
					
				case "Friday" :
					System.out.println(args[i] + ": weekday");
					break;
					
				case "Saturday" :
					System.out.println(args[i] + " : weekend");
					break;
					
				case "Sunday" :
					System.out.println(args[i] + " : weekend");
					break;
					
				default :
					System.out.println(args[i] + "not a correct day of week");
					break;
			}
		}
	}
}
