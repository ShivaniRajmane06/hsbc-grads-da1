public class MultiDimensionalArray {
	
	
	static int[][] matrix = new int[5][5];

	public static void createMatrix() {
	
		int num = 10;

		for(int i = 0; i < 5; i++) {
		
			for(int j = 0; j < 5; j++) {
			
				matrix[i][j] = num++;
			}
		}
	}

	public static void displayMatrix() {

                for(int i = 0; i < 5; i++) {

                        for(int j = 0; j < 5; j++) {

                                System.out.print("\t" + matrix[i][j]);
                        }
			System.out.println();
                }
        }

	public static void main(String[] args) {
	
		MultiDimensionalArray.createMatrix();
		MultiDimensionalArray.displayMatrix();
	}
}
