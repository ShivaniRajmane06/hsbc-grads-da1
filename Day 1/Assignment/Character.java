/*
	Class to demonstrate Vowels and Consonants.
*/

public class Character {

	public static void main(String[] args) {
		
		char ch = args[0].charAt(0);
		if(ch == 65 || ch == 69 || ch == 73 || ch == 79 || ch == 85 || ch == 97 || ch == 101 || ch == 105 || ch == 111 || ch == 117) {
		
			System.out.println(ch + " is a Vowel.");
		} else {
		
			System.out.println(ch + " is a Consonant.");
		}
	}
}
