public class Datatypes {

	public static void main(String[] args) {
		
		// declaration
		int value;

		// assignment
		// value = 56;
		
		// inline assignment of data to a variable
		short noOfDaysInAYear = 365;

		char ch = 'S';
		boolean b = true;

	}
}
