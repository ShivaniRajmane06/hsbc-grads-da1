public class Variables {

	public static void main(String[] args) {
	
		//variable names
		//rules
		/*
		 * 1. variable name can be alphanumeric
		 * 2. only _ and $ are allowed
		 * 3. you cannot start a variable name with a digit.
		 * 4. you cannot use a reserve/keyword.
		 * 5. java is case sensitive.
		 */

		//conventions
		/*
		 * 1. use descriptive variable names
		 * 	do not use variable names like i ,j ,k
		 * 2. use camelCase
		 * 3. Avoid using _ and $
		 */
	}
}
	
