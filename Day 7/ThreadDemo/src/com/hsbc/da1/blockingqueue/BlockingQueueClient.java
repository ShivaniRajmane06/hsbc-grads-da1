package com.hsbc.da1.blockingqueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BlockingQueueClient {

	public static void main(String[] args) {
		
		BlockingQueue<Message> message = new ArrayBlockingQueue<>(10);
		
		Producer producer = new Producer(message);
		Consumer consumer = new Consumer(message);
		
		new Thread(producer).start();
		new Thread(consumer).start();
		
		System.out.println(" ---Producer - Consumer thread started ... ");

	}

}
