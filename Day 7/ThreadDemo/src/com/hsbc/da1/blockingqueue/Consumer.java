package com.hsbc.da1.blockingqueue;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable{
	
	private BlockingQueue<Message> queue;
	
	public Consumer(BlockingQueue<Message> queue) {
		this.queue = queue;
	}
	
	@Override
	public void run() {
		consumeMessage();		
	}
	
	public void consumeMessage() {
		boolean flag = true;
		
		while(flag) {
			Message message = null;
			try {
				Thread.sleep(2000);
				message = this.queue.take();
				if(message.getCommand().equals("stop"))
					flag = false;
				else
					System.out.println("Message is : " + message.getCommand());
			}catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
