package com.hsbc.da1.blockingqueue;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable{
	
	private BlockingQueue<Message> queue;
	
	public Producer(BlockingQueue<Message> queue) {
		this.queue = queue;
	}
	
	@Override
	public void run() {
		produceMessage();
	
	}
	
	public void produceMessage() {
		for(int i = 0 ; i < 5; i++) {
			Message message = new Message (Math.random() * 1000 + "");
			
			try {
				Thread.sleep(2000);
				
				if(i == 4) {
					this.queue.put(new Message("stop"));
				} else {
					this.queue.put(message);
				}
				System.out.println("Meesage is in the queue .. ");
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}
	}
}
