package com.hsbc.da1.jdbc;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCDemo {

	public static void main(String[] args) {
		
		try (
				Connection connection = DriverManager.getConnection("jdbc:derby://localhost:1527/MyDb", "shivani", "shivani");
				Statement stmt = connection.createStatement();
				) {
			
			
			//int count = stmt.executeUpdate("insert into items(name, price) values ('Dress', 1000)");
			//count = stmt.executeUpdate("insert into items(name, price) values ('Shoes', 1500)");
			
			//System.out.println("No of items inserted " + count);
			
			ResultSet resultSet = stmt.executeQuery("select * from items");
			
			while(resultSet.next()) {
				System.out.print("Name : " + resultSet.getString(1));
				System.out.println("\tPrice : " + resultSet.getDouble(2));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}

}
