package com.hsbc.da1.client;

import java.util.List;

import com.hsbc.da1.controller.*;
import com.hsbc.da1.exception.*;
import com.hsbc.da1.model.*;

public class SavingsAccountClient {

	public static void main(String[] args) throws InsufficientBalanceException{
		
		SavingsAccountController accountController = new SavingsAccountController();
		
		Address shivaniAddress = new Address("Pune", 411041);
		SavingsAccount shivaniSavingsAccount = accountController.openSavingsAccount("Shivani", 100000, shivaniAddress);
		
		Address mayuriAddress = new Address("Pune", 411041);
		SavingsAccount mayuriSavingsAccount = accountController.openSavingsAccount("Mayuri", 100000, mayuriAddress);
		
		System.out.println("Account Id : " + shivaniSavingsAccount.getAccountNumber());
		System.out.println("Account Id : " + mayuriSavingsAccount.getAccountNumber());
		
		try {
			System.out.println("AccountBalance for : " + shivaniSavingsAccount.getAccountNumber() + " after deposit is : " + accountController.deposit(10001, 5000));
		}catch(UserNotFoundException une) {
			System.out.println(une.getMessage());
		}
		List<SavingsAccount> savingsAccounts = accountController.fetchSavingsAccount();
		for(SavingsAccount savingsAccount : savingsAccounts) {
			if(savingsAccount != null) {
				System.out.println("Account Id : " + savingsAccount.getAccountNumber());
				System.out.println("Customer Name : " + savingsAccount.getAccountNumber());
			}
		}
		
		// InsufficientBalanceException Example code
		try {
			System.out.println("Withdraw : " + accountController.withdraw(10001, 1000000));
		}catch(InsufficientBalanceException | UserNotFoundException une) {
			System.out.println(une.getMessage());
		}
		
		// USerNotFoundException example code
		
		try {
			System.out.println("User Account Information : " + accountController.fetchSavingsAccountById(10001).getCustomerName());
			System.out.println("User Account Information : " + accountController.fetchSavingsAccountById(10011).getCustomerName());
		}catch(UserNotFoundException une) {
			System.out.println(une.getMessage());
		}
	}

}
