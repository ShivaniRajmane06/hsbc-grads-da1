package com.hsbc.da1.model;

public class SavingsAccount {

		private String customerName;
		
		private long accountNumber;
		
		private double accountBalance;
		
		private static long counter = 10000;
		
		private Address address;
		
		public SavingsAccount(String customerName, double accountBalance) {
			this.customerName = customerName;
			this.accountBalance = accountBalance;
			
		}
		
		public  SavingsAccount(String customerName, double accountBalance, Address address) {
			this.customerName = customerName;
			this.accountBalance = accountBalance;
			this.address = address;
		}
		
		public  SavingsAccount(long accountNumber,String customerName, double accountBalance) {
			this.customerName = customerName;
			this.accountBalance = accountBalance;
			this.accountNumber = accountNumber;
			this.address = address;
		}
		
		public String getCity() {
			return this.address.getCity();
		}
		public int getZipCode() {
			return this.address.getZipCode();
		}

		public String getCustomerName() {
			return customerName;
		}

		public long getAccountNumber() {
			return accountNumber;
		}

		public double getAccountBalance() {
			return accountBalance;
		}
		public double setAccountBalance(double amount) {
			this.accountBalance = amount;
			return accountBalance;
		}
		
}
