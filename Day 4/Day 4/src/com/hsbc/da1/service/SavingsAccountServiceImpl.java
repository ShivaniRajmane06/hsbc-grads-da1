package com.hsbc.da1.service;

import java.util.*;

import com.hsbc.da1.dao.*;
import com.hsbc.da1.exception.*;
import com.hsbc.da1.model.Address;
import com.hsbc.da1.model.SavingsAccount;

public class SavingsAccountServiceImpl implements SavingsAccountService {
	
	private SavingsAccountDAO dao = new JDBCBackedSavingsAccountDAOImpl();
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance) {
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance);
		SavingsAccount newAcc = dao.saveSavingsAccount(savingsAccount);
		return newAcc;
	}
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, Address address) {
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance, address);
		return this.dao.saveSavingsAccount(savingsAccount);
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		this.dao.deleteSavingsAccount(accountNumber);
	}
	
	public List<SavingsAccount> fetchSavingsAccount() {
		List<SavingsAccount> savingsAccounts = this.dao.fetSavingsAccount();
		return savingsAccounts;
	}
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws UserNotFoundException{
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountById(accountNumber);
		return savingsAccount;
	}
	
	public double withdraw(long accountId, double amount) throws InsufficientBalanceException, UserNotFoundException{
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountById(accountId);
		if(savingsAccount.getAccountBalance() > (amount + 10000)) {
			savingsAccount.setAccountBalance(savingsAccount.getAccountBalance() - amount);
			this.dao.updateSavingsAccount(accountId, savingsAccount);
			return amount;
		}else {
			throw new InsufficientBalanceException("You do not have sufficient balance.");
		}
	}
	
	public double deposit(long accountId, double amount) throws UserNotFoundException{
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountById(accountId);
		if(savingsAccount != null) {
			savingsAccount.setAccountBalance(savingsAccount.getAccountBalance() + amount);
			this.dao.updateSavingsAccount(accountId, savingsAccount);
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}
	
	public double checkBalance(long accountId) throws UserNotFoundException{
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountById(accountId);
		return savingsAccount.getAccountBalance();
	}

}
