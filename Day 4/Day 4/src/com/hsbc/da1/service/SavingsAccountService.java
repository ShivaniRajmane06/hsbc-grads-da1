package com.hsbc.da1.service;
import com.hsbc.da1.model.*;
import java.util.*;
import com.hsbc.da1.exception.*;

public interface SavingsAccountService {
	
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance);
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, Address address);
	
	public void deleteSavingsAccount(long accountNumber);
	
	public List<SavingsAccount> fetchSavingsAccount();
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws UserNotFoundException;
	
	public double withdraw(long accountId, double amount) throws InsufficientBalanceException, UserNotFoundException;
	
	public double deposit(long accountId, double amount) throws UserNotFoundException;
	
	public double checkBalance(long accountId) throws UserNotFoundException;
	
	
	
}
