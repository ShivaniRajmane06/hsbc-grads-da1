package com.hsbc.da1.exception;

public class InsufficientBalanceException extends Exception {
	
	public InsufficientBalanceException(String message) {
		super(message);
	}
	
	public String getMessage() {
		return super.getMessage();
	}

}
