package com.hsbc.da1.dao;
import com.hsbc.da1.model.*;

import java.util.List;

import com.hsbc.da1.exception.*;

public interface SavingsAccountDAO {
	
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount);
	
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount);
	
	public void deleteSavingsAccount(long accountNumber);
	
	public List<SavingsAccount> fetSavingsAccount();
	
	public SavingsAccount fetchSavingsAccountById(long accountNumber) throws UserNotFoundException;
		
}
	

