package com.hsbc.da1.dao;
import com.hsbc.da1.model.SavingsAccount;

import java.util.Arrays;
import java.util.List;

import com.hsbc.da1.exception.*;

public class ArrayBackedSavingsAccountDAOImpl implements SavingsAccountDAO {
	
	private static SavingsAccount[] savingsAccounts= new SavingsAccount[20];
	private static int counter = 0;
	
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		this.savingsAccounts[counter++] = savingsAccount;
		return savingsAccount;
	}
	
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		for(int index = 0; index < savingsAccounts.length; index++) {
			if(this.savingsAccounts[index].getAccountNumber() == accountNumber) {
				this.savingsAccounts[index] = savingsAccount;
				break;
			}
		}
		return savingsAccount;
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		for(int index = 0; index < savingsAccounts.length; index++) {
			if(this.savingsAccounts[index].getAccountNumber() == accountNumber) {
				this.savingsAccounts[index] = null;
				break;
			}
		}
	}
	
	public List<SavingsAccount> fetSavingsAccount() {
		return Arrays.asList(savingsAccounts);
	}
	
	public SavingsAccount fetchSavingsAccountById(long accountNumber) throws UserNotFoundException{
		for(int index = 0; index < savingsAccounts.length; index++) {
			if(this.savingsAccounts[index]!= null && this.savingsAccounts[index].getAccountNumber() == accountNumber) {
				return this.savingsAccounts[index];
			}
		}
		throw new UserNotFoundException("User Not Found");
	}
}
