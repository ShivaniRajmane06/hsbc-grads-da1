package com.hsbc.da1.dao;
import com.hsbc.da1.model.SavingsAccount;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.hsbc.da1.exception.*;

public class LinkedListBackedSavingsAccountDAOImpl implements SavingsAccountDAO {
	
	private static List<SavingsAccount> savingsAccounts= new LinkedList<>();
	//private static int counter = 0;
	
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		this.savingsAccounts.add(savingsAccount);
		return savingsAccount;
	}
	
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		for(SavingsAccount sa : savingsAccounts) {
			if(sa.getAccountNumber() == accountNumber) {
				sa = savingsAccount;
				break;
			}
		}
		return savingsAccount;
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		for(SavingsAccount sa : savingsAccounts) {
			if(sa.getAccountNumber() == accountNumber) {
				this.savingsAccounts.remove(sa);
				break;
			}
		}
	}
	
	public List<SavingsAccount> fetSavingsAccount() {
		return savingsAccounts;
	}
	
	public SavingsAccount fetchSavingsAccountById(long accountNumber) throws UserNotFoundException{
		for(SavingsAccount sa : savingsAccounts) {
			if(sa != null && sa.getAccountNumber() == accountNumber) {
				return sa;
			}
		}
		throw new UserNotFoundException("User Not Found");
	}
}
