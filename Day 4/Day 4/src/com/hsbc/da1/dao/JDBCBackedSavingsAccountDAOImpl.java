package com.hsbc.da1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.derby.client.am.SqlException;

import com.hsbc.da1.exception.UserNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public class JDBCBackedSavingsAccountDAOImpl implements SavingsAccountDAO{ 
	

		private static final String INSERT_QUERY = "insert into savingsAccount (customername,accountbalance,city,zipcode) values (?,?,?,?)";
		private static final String UPDATE_QUERY = "update savingsAccount set accountbalance = ? where accountid = ?";
		private static final String DELETE_QUERY = "delete from savingsAccount where accountid = ?";
		private static final String SELECT_BY_ID = "select * from savingsAccount where accountid = ?";
		private static final String SELECT_QUERY = "select * from savingsAccount";
		
		
		
		private static Connection getConnection() {
			try {
				Connection connection = DriverManager.getConnection("jdbc:derby://localhost:1527/mydb", "shivani", "shivani");
				return connection;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
				
		@Override
		public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
			
			Connection connection = getConnection();
			try {
				PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY);
				preparedStatement.setString(1, savingsAccount.getCustomerName());
				preparedStatement.setDouble(2, savingsAccount.getAccountBalance());
				preparedStatement.setString(3, savingsAccount.getCity());
				preparedStatement.setInt(4, savingsAccount.getZipCode());
				int rowsUpdated = preparedStatement.executeUpdate();
				if(rowsUpdated > 0) {
					return savingsAccount;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Account not created");
			return null;
		}
		
		@Override
		public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
			Connection connection = getConnection();
			
			try {
				PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY);
				preparedStatement.setDouble(1, savingsAccount.getAccountBalance());
				preparedStatement.setLong(2, accountNumber);


				int rowsUpdated = preparedStatement.executeUpdate();
				return savingsAccount;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Account not updated");
			return null;
		}
		@Override
		public void deleteSavingsAccount(long accountNumber) {
			
			Connection connection = getConnection();
			try {
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY);
				preparedStatement.setDouble(1, accountNumber);

				int rowsUpdated = preparedStatement.executeUpdate();
				if(rowsUpdated > 0)
					return;
			}catch(SQLException e) {
				e.printStackTrace();
			}
			System.out.println("Account not deleted.");
		}
		
		
		@Override
		public List<SavingsAccount> fetSavingsAccount() {
			Connection connection = getConnection();
			List<SavingsAccount> savingsAccountSet = new ArrayList<>();
			try {
				PreparedStatement preparedStatement = connection.prepareStatement(SELECT_QUERY);
				ResultSet rs = preparedStatement.executeQuery();
				while (rs.next()) {
					SavingsAccount savingsAccount = new SavingsAccount(rs.getLong("accountid"),
							rs.getString("customername"), rs.getDouble("accountbalance"));
					savingsAccountSet.add(savingsAccount);
				}
				return savingsAccountSet;
			} catch (SQLException e) {
				e.printStackTrace();
			}
			System.out.println("Not fetched");
			return null;
			
		}
		
		
		@Override
		public SavingsAccount fetchSavingsAccountById(long accountNumber) throws UserNotFoundException {
			
			Connection connection = getConnection();
			try {
				PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID);
				ResultSet rs = preparedStatement.executeQuery();
				if(rs.next()) {
					return new SavingsAccount(rs.getLong("accountid"),
							rs.getString("customername"), rs.getDouble("accountbalance"));
				}
			}catch(SQLException e) {
				e.printStackTrace();
			}
			System.out.println("Account not fetched");
			return null;
		}		
}
