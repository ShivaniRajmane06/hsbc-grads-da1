package com.hsbc.da1.controller;
import java.util.List;

import com.hsbc.da1.exception.*;
import com.hsbc.da1.model.*;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.service.SavingsAccountServiceImpl;

public class SavingsAccountController {
	
	
private SavingsAccountService savingsAccountService = new SavingsAccountServiceImpl();
	
	public SavingsAccount openSavingsAccount(String customerName, double amount) {
		SavingsAccount savingsAccount = this.savingsAccountService.createSavingsAccount(customerName, amount);
		return savingsAccount;
	}
	
	public SavingsAccount openSavingsAccount(String customerName, double amount, Address address) {
		SavingsAccount savingsAccount = this.savingsAccountService.createSavingsAccount(customerName, amount, address);
		return savingsAccount;
	}
	
	public void deleteSavingsAccount(long accountId) {
		this.savingsAccountService.deleteSavingsAccount(accountId);
	}
	
	public List<SavingsAccount> fetchSavingsAccount() {
		List<SavingsAccount> savingsAccount = this.savingsAccountService.fetchSavingsAccount();
		return savingsAccount;
	}
	
	public SavingsAccount fetchSavingsAccountById(long accountId) throws UserNotFoundException{
		SavingsAccount savingsAccount = this.savingsAccountService.fetchSavingsAccountByAccountId(accountId);
		return savingsAccount;
	}
	
	public double withdraw(long accountId, double amount) throws InsufficientBalanceException, UserNotFoundException{
		double updatedAmount = this.savingsAccountService.withdraw(accountId, amount);
		return updatedAmount;
	}
	
	public double deposit(long accountId, double amount) throws UserNotFoundException {
		return this.savingsAccountService.deposit(accountId, amount);
	}
	
	public double checkBalance(long accountId) throws UserNotFoundException {
		return this.savingsAccountService.checkBalance(accountId);
	}

}
