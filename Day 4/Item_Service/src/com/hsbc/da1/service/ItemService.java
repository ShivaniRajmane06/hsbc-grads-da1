package com.hsbc.da1.service;
import com.hsbc.da1.model.*;
import java.util.*;

public interface ItemService {
	
	public Item createItem(String itemName, double price);
	
	public void deleteItemById(long itemID);
	
	public List<Item> fetchAllItems();
	
	public Item fetchItemById(long itemId);
	
	public Item updateItem(long itemId, Item item);	
	
}
