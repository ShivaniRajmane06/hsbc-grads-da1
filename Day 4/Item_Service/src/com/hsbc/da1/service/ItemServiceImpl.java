package com.hsbc.da1.service;

import com.hsbc.da1.model.Item;
import java.util.*;
import com.hsbc.da1.dao.*;

public class ItemServiceImpl implements ItemService{
	
	ItemDAOImpl dao = new ItemDAOImpl();

	@Override
	public Item createItem(String itemName, double price) {
		Item item = this.dao.saveItem(itemName, price);
		return item;
	}

	@Override
	public void deleteItemById(long itemID) {
		this.dao.deleteItemById(itemID);;
		
	}

	@Override
	public List<Item> fetchAllItems() {
		
		return this.dao.fetchAllItems();
	}

	@Override
	public Item fetchItemById(long itemId) {
		Item item = this.dao.fetchItemByID(itemId);
		return item;
	}

	@Override
	public Item updateItem(long itemId, Item item) {
		
		Item newitem = this.dao.updateItem(itemId, item);
		return newitem;
	}

}
