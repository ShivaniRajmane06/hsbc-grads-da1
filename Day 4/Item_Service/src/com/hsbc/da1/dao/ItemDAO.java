package com.hsbc.da1.dao;
import com.hsbc.da1.model.*;
import java.util.*;

public interface ItemDAO {
	
	public Item saveItem(String itemName, double price);
	
	public Collection<Item> fetchAllItems();
	
	public Item fetchItemByID(long itemId);
	
	public void deleteItemById(long itemId);
	
	public Item updateItem(long ItemId, Item item);
}
