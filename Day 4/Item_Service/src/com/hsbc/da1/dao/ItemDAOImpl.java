package com.hsbc.da1.dao;

import com.hsbc.da1.model.Item;
import java.util.*;
import java.io.*;

public class ItemDAOImpl implements ItemDAO{
	
	private static List<Item> items = new ArrayList<>();
	
	@Override
	public Item saveItem(String itemName, double price) {
		Item item = new Item(itemName, price);
		this.items.add(item);
		try {
			FileOutputStream fos = new FileOutputStream("Output.txt");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(items);
			
			oos.close();
			fos.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
		return item;
	}

	@Override
	public List<Item> fetchAllItems() {
		return items;
	}

	@Override
	public Item fetchItemByID(long itemId) {
		for(Item item : items) {
			if(item.getItemId() == itemId) {
				return item;
			}
		}
		return null;
	}

	@Override
	public void deleteItemById(long itemId) {
		for(Item item : items) {
			if(item.getItemId() == itemId) {
				items.remove(item);
			}
		}
	}

	@Override
	public Item updateItem(long itemId, Item item) {
		for(Item i : items) {
			if(i.getItemId() == itemId) {
				i = item;
			}
		}
		return item;
	}
	

}
