package com.hsbc.da1.controller;
import com.hsbc.da1.service.*;
import java.util.*;
import com.hsbc.da1.model.*;

public class ItemController {
	
	ItemServiceImpl itemServiceImpl = new ItemServiceImpl();
	
	public Item createItem(String itemName, double price) {
		Item item = this.itemServiceImpl.createItem(itemName, price);
		return item;
	}
	
	public void deleteItem(long itemId) {
		this.itemServiceImpl.deleteItemById(itemId);
	}
	
	public List<Item> fetchAllItems() {
		return this.itemServiceImpl.fetchAllItems();
	}
	
	public Item fetchItemById(long itemId) {
		Item item = this.itemServiceImpl.fetchItemById(itemId);
		return item;
	}
	
	public Item updateItem(long itemId, Item item) {
		return this.itemServiceImpl.updateItem(itemId, item);
	}
}
