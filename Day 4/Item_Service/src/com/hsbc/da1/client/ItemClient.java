package com.hsbc.da1.client;
import com.hsbc.da1.controller.*;
import java.io.*;
import com.hsbc.da1.model.*;
import java.util.*;

public class ItemClient {
	
	public static void main(String[] args) {
		ItemController controller = new ItemController();
		
		Item mobile = controller.createItem("Samsung M31", 18000);
		Item laptop = controller.createItem("Dell 5000", 40000);
		
		System.out.println("Item Name : " + mobile.getItemName());
		System.out.println("Item ID : " + mobile.getItemId());
		
		System.out.println("Item Name : " + laptop.getItemName());
		System.out.println("Item ID : " + laptop.getItemId());
		
		System.out.println("\nAfter Serialization");
		try {
			FileInputStream fis = new FileInputStream("Output.txt");
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			List<Item> list = (ArrayList<Item>) ois.readObject();
			for(Item item : list) {
				System.out.println(item.getItemName() + " : " +item.getItemId());
			}
			//System.out.println((ArrayList<Item>) ois.readObject());
		} catch (FileNotFoundException e) {e.printStackTrace();} 
		  catch (ClassNotFoundException e) {e.printStackTrace();}
		  catch (IOException e) {e.printStackTrace();}
			
		
		
	}
	
}
