package com.hsbc.da1.model;

import java.io.Serializable;

public class Item implements Serializable{
	
	private String itemName;
	
	private long itemId;
	
	private double price;
	
	private static long counter = 1000;
	
	public Item(String itemName, double price) {
		this.itemId = ++counter;
		this.itemName = itemName;
		this.price = price;
	}

	public String getItemName() {
		return itemName;
	}

	public long getItemId() {
		return itemId;
	}

	public double getPrice() {
		return price;
	}
	
}
