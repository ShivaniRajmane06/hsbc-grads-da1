abstract class BankAccount {

	public static long accountNumberTracker = 10000;

	public String customerName;

	public final long accountNumber;

	public double accountBalance;

	public BankAccount(String customerName, double accountBalance) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.accountNumber = ++accountNumberTracker;
	}

	public abstract double withdraw(double amount);
	public abstract boolean validateBalance(double amount);
	public abstract void grantLoan(double amount);

	public final double getAccountBalance() {
		return this.accountBalance;
	}
	
	public final double deposit(double amount) {
		this.accountBalance += amount;
		return amount;
	}
}

final class SavingsAccount extends BankAccount {

		public SavingsAccount(String customerName, double accountBalance) {
			super(customerName, accountBalance);
			
		}

		public final boolean validateBalance(double amount) {
			if(this.accountBalance > (amount + 10000) && amount <= 10000)
				return true;
			else
				return false;
		}

		public final double withdraw(double amount) {
			if(validateBalance(amount)) {
				this.accountBalance -= amount;
				return amount;
			}
			return 0;
		}

		public final void grantLoan(double amount) {
			if(amount <= 500000)
				System.out.println("Your Saving Account is eligible for Loan of amount + Rs." + amount);
			else 
				System.out.println("Your Saving Account is not eligible for Loan for amount greater than Rs. 5L. ");
		}

}

final class CurrentAccount extends BankAccount {

	private String businessName;
	private String gstNumber;

	public CurrentAccount(String customerName, double accountBalance, String businessName, String gstNumber) {
			super(customerName, accountBalance);

			this.businessName = businessName;
			this.gstNumber = gstNumber;
	}

	public final boolean validateBalance(double amount) {
			if(this.accountBalance > (amount + 25000))
				return true;
			else
				return false;
	}

		public final double withdraw(double amount) {
			if(validateBalance(amount)) {
				this.accountBalance -= amount;
				return amount;
			}
			return 0;
	}

	public final void grantLoan(double amount) {
			if(amount <= 2500000)
				System.out.println("Your Saving Account is eligible for Loan of amount + Rs." + amount);
			else 
				System.out.println("Your Saving Account is not eligible for Loan for amount greater than Rs. 5L. ");
	}
}

final class SalariedAccount extends BankAccount {

	public SalariedAccount(String customerName, double accountBalance) {
			super(customerName, accountBalance);
	}

	public final boolean validateBalance(double amount) {
			if(this.accountBalance >= amount && amount <= 15000)
				return true;
			else
				return false;
	}

	public final double withdraw(double amount) {
			if(validateBalance(amount)) {
				this.accountBalance -= amount;
				return amount;
			}
			return 0;
	}

	public final void grantLoan(double amount) {
			if(amount <= 1000000)
				System.out.println("Your Saving Account is eligible for Loan of amount Rs." + amount);
			else 
				System.out.println("Your Saving Account is not eligible for Loan for amount greater than Rs. 5L. ");
		}
}

public class Banking {

	public static void main(String[] args) {

		BankAccount account = null;
		String choice = args[0];

		switch(choice) {
			case "1" :
						account = new SavingsAccount("Shivani Rajmane", 50000);
						System.out.println("Balance Before withdrawal : " + account.getAccountBalance());
						account.withdraw(2000);
						System.out.println("Balance after withdrawal : " + account.getAccountBalance());
						break;

			case "2" :
						account = new CurrentAccount("Mayuri", 50000, "Deutsche", "72CWHDGSH12H1");
						account.grantLoan(500000);
						break;

			case "3" :
						account = new SalariedAccount("Rutuja", 100000);
						account.grantLoan(5000000);
						break;

			default :
						System.out.println("Enter valid operation");
						break;

		}
	}
}