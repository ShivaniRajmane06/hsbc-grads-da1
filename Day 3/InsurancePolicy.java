interface Insurance {
	
	double calculatePremium(String vehicleName, double amount, String model);
	long payPremium(String vehicleNumber, double premiumAmount, String model);
}

class SavingAccount {
	public final String customerName ;
	public final long accountNumber;
	public double accountBalance;
	static long accountNumberTracker = 1000;

	public SavingAccount(String customerName, double accountBalance) {
		this.customerName = customerName;
		this.accountNumber = ++accountNumberTracker;
		this.accountBalance = accountBalance;
	}

	public final boolean validateBalance(double amount) {
		if(this.accountBalance > (amount + 10000))
			return true;
		else
			return false;
	}

	public final boolean withdraw(double amount) {
		if(validateBalance(amount)) {
			this.accountBalance -= amount;
			return true;
		}else
		return false;
	}

}

class TataAIG implements Insurance {

	static long policyNumber = 10110101;

	public double calculatePremium(String vehicleName, double amount, String model) {
		System.out.println("Welcome to TataAIG.");
		System.out.println("Your premium for Vehical : " + vehicleName + " , Model : " + model + " per month will be : " + (amount * 0.1));
		return (amount * 0.1);
	}

	public long payPremium(String vehicleNumber, double premiumAmount, String model) {
		
		SavingAccount shivani = new SavingAccount("Shivani", 100000);
		if(shivani.withdraw(premiumAmount)){
			System.out.println("You have successfully paid premium for this month for Vehicle Number " + vehicleNumber + ", Model " + model + ".\n Amount Paid : " + (premiumAmount));
			return (++policyNumber);
		} else {
			System.out.println("You do not have sufficient balance to pay your premium.");
		}
		return (++policyNumber);
	}
}


class BajajInsurance implements Insurance {

	static long policyNumber = 20110101;

	public double calculatePremium(String vehicleName, double amount, String model) {
		System.out.println("Welcome to BajajInsurance.");
		System.out.println("Your premium for Vehical : " + vehicleName + " , Model : " + model + " per month will be : " + (amount * 0.2));
		return (amount * 0.1);
	}

	public long payPremium(String vehicleNumber, double premiumAmount, String model) {
		
		SavingAccount shivani = new SavingAccount("Shivani", 100000);
		if(shivani.withdraw(premiumAmount)){
			System.out.println("You have successfully paid premium for this month for Vehicle Number " + vehicleNumber + ", Model " + model + ".\n Amount Paid : " + (premiumAmount));
			return (++policyNumber);
		} else {
			System.out.println("You do not have sufficient balance to pay your premium.");
		}
		return (++policyNumber);
	}
}

public class InsurancePolicy {
	public static void main(String[] args) {
		Insurance insurance = null;

		switch(args[0]) {
			case "1" :
					insurance = new TataAIG();
					break;

			case "2" :
					insurance = new BajajInsurance();
					break;

			default :
					System.out.println("Enter valid choice :( ");
					break;
		}
		
		if(insurance != null){
			insurance.calculatePremium("Swift Dzire", 500000, "LXi");
			System.out.println("Your Policy number is : " + insurance.payPremium("MH12 LF 7954", 30000, "LXi"));
		}
	}
}